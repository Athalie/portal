import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatDatepickerModule, MatNativeDateModule, MatFormFieldModule, MatIconModule, MatInputModule} from '@angular/material';
import { AppComponent } from './app.component';
import { LoginComponent } from './pages/login/login.component';
import { EnergyGraphComponent } from './pages/energy-graph/energy-graph.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { EnergyPieComponent } from './pages/energy-pie/energy-pie.component';
import { GeomapComponent } from './pages/geomap/geomap.component';
import { AppRoutingModule } from './app-routing.module';
import { AuthenticationService, AlertService, UserService, AppService } from './services';
import { AuthGuard, LoginGuard } from './helpers/guards';
import { AlertComponent } from './components/alert/alert.component';
import { NotAuthorizedInterceptor, EnergyPieInterceptor } from './helpers/interceptor';
import { MenuItemSortPipe } from './helpers/pipes/menu-item-sort.pipe';
import { RecommendationsComponent,ButtonViewComponent } from './pages/recommendations/recommendations.component';
import { ChartsComponent } from './pages/charts/charts.component';

@NgModule({
  entryComponents: [
    ButtonViewComponent,
  ],
  declarations: [
    AppComponent,
    LoginComponent,
    EnergyGraphComponent,
    EnergyPieComponent,
    GeomapComponent,
    DashboardComponent,
    AlertComponent,
    MenuItemSortPipe,
    RecommendationsComponent,
    ButtonViewComponent,
    ChartsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    Ng2SmartTableModule,
    NgxChartsModule,
    BrowserAnimationsModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule
  ],
  providers: [
    AuthGuard,
    LoginGuard,
    AuthenticationService,
    AlertService,
    UserService,
    AppService,
    CookieService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: NotAuthorizedInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: EnergyPieInterceptor,
      multi: true
    }
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule {
}
