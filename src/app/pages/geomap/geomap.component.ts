declare const L: any, Config: any;

import { Component, OnInit, OnDestroy, Renderer2 } from '@angular/core';
import { AppService } from '../../services';
import { greenIcon, greyIcon } from '../../../assets/global/vendor/leaflet/icons/leaflet-color-markers';

@Component({
  selector: 'app-geomap',
  templateUrl: './geomap.component.html',
  styleUrls: [ './geomap.component.scss' ]
})
export class GeomapComponent implements OnInit, OnDestroy {
  bodyClasses: string[] = 'page-map page-map-full'.split(' ');
  initialLat: number = 55.73448;
  initialLon: number = 37.61558;
  map: any;

  constructor(private renderer: Renderer2, private appService: AppService) {
  }

  ngOnInit() {
    this.bodyClasses.forEach(token => this.renderer.addClass(document.body, token));
    this.map = L.map('map', {
      center: [ this.initialLat, this.initialLon ],
      zoom: 11
    });
    L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(this.map);

    this.appService.getMapObjects().subscribe(data =>
        data.forEach(coord => {
          const marker = L.marker(coord.coords, { icon: (coord.isActive === 'True' ? greenIcon : greyIcon) });
          marker.addTo(this.map);
        }),
      error => console.log(error));
  }

  ngOnDestroy() {
    this.bodyClasses.forEach(token => this.renderer.removeClass(document.body, token));
  }
}
