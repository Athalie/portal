import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl, } from '@angular/platform-browser';
import { EnergyGraphService, UserService } from '../../services';

@Component({
  selector: 'app-energy-graph',
  templateUrl: './energy-graph.component.html',
  styleUrls: [ './energy-graph.component.scss' ]
})
export class EnergyGraphComponent implements OnInit {
  url: SafeResourceUrl;

  constructor(private userService: UserService, private energyGraphService: EnergyGraphService, private sanitizer: DomSanitizer) {
  }

  ngOnInit() {
    // this.energyGraphService.loadGrafana(`grafana/dashboards/home?user=${this.userService.getUserName()}`).subscribe(url =>
    //   this.url = this.sanitizer.bypassSecurityTrustResourceUrl(url)
    // );
  }
}
