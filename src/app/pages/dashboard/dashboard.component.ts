import { Component, Input, OnDestroy, OnInit, Renderer2 } from '@angular/core';
import { Router } from '@angular/router';
import { AppService, AuthenticationService, UserService } from '../../services';
import { TreeNode } from '../../models';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: [ './dashboard.component.scss' ]
})
export class DashboardComponent implements OnInit, OnDestroy {
  bodyClasses: string = "dashboard";
  @Input() menu: Array<TreeNode>;
  settings: any;
  userName: string = '';

  constructor(private renderer: Renderer2,
              private router: Router,
              private authenticationService: AuthenticationService,
              private appService: AppService,
              private userService: UserService) {
  }

  ngOnInit() {
    this.renderer.addClass(document.body, this.bodyClasses);  // TODO выкинуть document.body
    this.userName = this.userService.getUserToken();
    //TODO разобраться насчет unsubscribe
    this.appService.getMenu().subscribe(root => this.menu = root,
      error => console.log(error),
      () => this.appService.getSettings().subscribe(data => this.settings = data,
        () => this.settings = null
      )
    );
  }

  //TODO избавиться от костылей
  onExpand(node: TreeNode) {
    node.hasChildren && node.children.length === 0 && this.appService.getMenu(node).subscribe(nodes => {
      node && node.addChildren(nodes);
      node.expanded = !node.expanded;
    });
  }

  return() {
    this.router.navigate(['/dashboard'])
  }

  onLogout() {
    this.authenticationService.logout().subscribe(() => this.router.navigate([ '/login' ]));
  }

  ngOnDestroy() {
    this.renderer.removeClass(document.body, this.bodyClasses);
  }
}
