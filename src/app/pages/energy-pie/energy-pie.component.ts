import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { EnergyPieService } from '../../services';

@Component({
  selector: 'app-energy-pie',
  templateUrl: './energy-pie.component.html',
  styleUrls: ['./energy-pie.component.scss']
})
export class EnergyPieComponent implements OnInit {
  url:String = '';

  constructor(private energyPieService: EnergyPieService, public sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.energyPieService.createPieLink().subscribe(response => {this.url = URL.createObjectURL(response)})
  }
}
