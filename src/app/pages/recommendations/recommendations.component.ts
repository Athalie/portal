import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ViewCell } from 'ng2-smart-table';

interface Value {
  type: string;
  content: string
}

@Component({
  selector: 'button-view',
  template: `<button (click)="onClick()" *ngIf="value.type === 'button'">{{ value.content }}</button>
  <p *ngIf="value.type !== 'button'">{{ value.content }}</p>
    
  `,
})
export class ButtonViewComponent implements OnInit {
  @Input() value: Value;
  @Input() rowData: any;

  @Output() save: EventEmitter<any> = new EventEmitter();

  ngOnInit() {
  }

  onClick() {
    this.save.emit(this.rowData);
  }
}

@Component({
  selector: 'app-recommendations',
  templateUrl: './recommendations.component.html',
  styleUrls: [ './recommendations.component.scss' ]
})
export class RecommendationsComponent implements OnInit {

  settings = {
    noDataMessage: "Нет данных особытиях",
    actions: {
      columnTitle: "",
      add: false,
      edit: false,
      delete: false
    },
    columns: {
      recommendation: {
        title: 'Рекомендация',
        type: "text"
      },
      read: {
        title: `Прочитано (пользователь, время)`,
        type: 'custom',
        renderComponent: ButtonViewComponent
      },
      status_in: {
        title: 'Статус и время активации',
        type: "text"
      },
      status_out: {
        title: 'Стaтус и время дезактивации',
        type: 'custom',
        renderComponent: ButtonViewComponent
      }
    }
  };

  data = [
    {
      recommendation: "Рекомендация 1",
      read: { type: "text", content: "kfc1, 12.10.2018 11:50" },
      status_in: 'Активирована событием 12.10.2018 10:50',
      status_out: { type: "button", content: "Отказаться" },
    },
    {
      recommendation: "Рекомендация 2",
      read: { type: "text", content: "kfc1, 12.10.2018 11:50" },
      status_in: 'Активирована событием 12.10.2018 10:50',
      status_out: { type: "text",  content: 'Дезактивирована пользователем kfc1 12.10.2018 12:00'},
    },
    {
      recommendation: "Рекомендация 3",
      read: { type: "button", content: "Прочитать" },
      status_in: 'Активирована событием 14.10.2018 13:16',
      status_out: { type: "button", content: "Отказаться" },
    }
  ];


  constructor() {
  }

  ngOnInit() {
  }

}
