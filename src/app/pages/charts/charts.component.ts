import { Component, OnInit } from '@angular/core';

const DATA = [
  {
    "name": "Первая",
    "value": 5.23
  },
  {
    "name": "Третья",
    "value": 5.59
  },
  {
    "name": "Четвертая",
    "value": 2.99
  }
];

@Component({
  selector: 'app-charts',
  templateUrl: './charts.component.html',
  styleUrls: [ './charts.component.scss' ]
})
export class ChartsComponent implements OnInit {
  single: any[];
  multi: any[];

  view: any[] = [ 1400, 600 ];

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = false;
  showXAxisLabel = true;
  xAxisLabel = 'Категории';
  showYAxisLabel = true;
  yAxisLabel = 'Стоимость кВтч';

  colorScheme = {
    domain: [ '#5AA454', '#A10A28', '#C7B42C', '#AAAAAA' ]
  };

  constructor() {
    Object.assign(this, { DATA })
  }

  ngOnInit() {

  }

  onSelect(event) {
    console.log(event);
  }

}
