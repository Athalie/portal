import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GeomapComponent } from './pages/geomap/geomap.component';
import { EnergyPieComponent } from './pages/energy-pie/energy-pie.component';
import { EnergyGraphComponent } from './pages/energy-graph/energy-graph.component';
import { RecommendationsComponent } from './pages/recommendations/recommendations.component';
import { ChartsComponent } from './pages/charts/charts.component';
import { LoginComponent } from './pages/login/login.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { AuthGuard, LoginGuard } from './helpers/guards';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent, canActivate: [ LoginGuard ] },
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [ AuthGuard ],
    children: [
      { path: '', component: GeomapComponent },
      { path: 'energy_pie', component: EnergyPieComponent },
      { path: 'energy_graph', component: EnergyGraphComponent },
      { path: 'e0', component: RecommendationsComponent },
      { path: 'charts', component: ChartsComponent }
    ]
  },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule {
}
