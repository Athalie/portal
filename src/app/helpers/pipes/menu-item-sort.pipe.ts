import { Pipe, PipeTransform } from '@angular/core';
import { TreeNode } from '../../models';

@Pipe({
  name: 'menuItemSort'
})
export class MenuItemSortPipe implements PipeTransform {
  transform(menuItems: TreeNode[]) {
    return menuItems && menuItems.sort((itemFirst, itemSecond) => itemFirst.position - itemSecond.position);
  }

}
