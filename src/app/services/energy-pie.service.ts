import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EnergyPieService {

  constructor(private http: HttpClient) {
  }

  createPieLink() {
    return this.http.get('/energy-pie', {responseType: 'blob'})
  }
}
