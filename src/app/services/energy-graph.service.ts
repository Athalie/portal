import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RedirectUri } from '../models';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EnergyGraphService {

  constructor(private http: HttpClient) {
  }

  loadGrafana(url: string) {
    //TODO вынести конвертирование
    return this.http.get<RedirectUri>(url)
      .pipe(map(response => response ? `/grafana${response.redirectUri}` : url))
  }
}
