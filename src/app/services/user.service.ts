import {Injectable} from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class UserService {
  getUserToken() {
    return JSON.parse(localStorage.getItem('currentUser')).name;
  }


}
