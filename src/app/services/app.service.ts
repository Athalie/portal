import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, catchError, tap } from 'rxjs/operators';
import { ITopLevelMenuItem, TreeNode, IMapObject } from '../models';
import { COORD_SYSTEM_NAME, IS_ACTIVE_SYSTEM_NAME } from './constants';
import { of } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AppService {
  constructor(private http: HttpClient) {
  }

  getMenu(parent = null) {
    return this.http.get<ITopLevelMenuItem[]>(`/portal/rest/left_menu?id=${parent ? parent.id : null}`, { withCredentials: true })
      .pipe(map(menu => menu.map(item => new TreeNode(item, parent))))
  }

  getSettings() {
    return this.http.get<any>('/portal/rest/setting/', { withCredentials: true })
      .pipe(tap(catchError(err => {
        if (err.status === 403) {
          return of(err);
        }
      })))
  }

  getMapObjects() {
    return this.http.get<IMapObject[]>('/portal/rest/map/', { withCredentials: true })
      .pipe(map(mapObjects => mapObjects.reduce((coords, mapObj) => [ ...coords,
        ...mapObj.client_objects.map(obj =>
          ({
            coords: obj.parameters_value[ COORD_SYSTEM_NAME ].split(","),
            isActive: obj.parameters_value[ IS_ACTIVE_SYSTEM_NAME ]
          })) ], [])))
  }
}
