import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';
import { User } from '../models/User';

@Injectable()
export class AuthenticationService {
  constructor(private http: HttpClient, private cookies: CookieService) {
  }

  login(user: User) {
    return this.http.post<any>(`/portal/login/`, user).pipe(map(
      () => localStorage.setItem('currentUser', JSON.stringify({
        name: user.email.substr(0, user.email.indexOf('@')),
        tokens: this.cookies.getAll()
      })),
      error => console.log(error))
    );
  }

  logout() {
    return this.http.get(`/portal/logout/`)
      .pipe(tap(
        () => localStorage.removeItem('currentUser'),
        catchError(err => {
          console.log(err);
          return err
        })
      ));
  }
}
