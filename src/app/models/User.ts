export interface User {
  email: string;
  password: string;
}

export interface IGraphUser {
  login: string;
  password: string;
}

export class User {
  email = '';
  password = '';

  constructor(user: User) {
    this.email = user.email;
    this.password = user.password;
  }
}

export class GraphUser {
  login = '';
  password = '';

  constructor(user: IGraphUser) {
    this.login = user.login;
    this.password = user.password;
  }
}
