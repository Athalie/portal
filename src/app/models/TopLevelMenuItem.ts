export interface ITopLevelMenuItem {
  has_child: boolean;
  id: string;
  name: string;
  number: number;
  system_name: string;
  active: boolean;
  source: string;
}

export class TreeNode {
  expanded: boolean;
  children: Array<TreeNode>;

  constructor(private node: ITopLevelMenuItem, public parent: TreeNode) {
    this.expanded = false;
    this.children = [];
  }

  get id() {
    return this.node.id;
  }

  get title() {
    return this.node.name;
  }

  get sName() {
    return this.node.system_name;
  }

  get isActive() {
    return this.node.active === undefined ? true : this.node.active;
  }

  get hasChildren() {
    return this.node.has_child;
  }

  get position() {
    return this.node.number;
  }

  addChildren(nodes: Array<TreeNode>) {
    this.children = nodes;
  }
}
