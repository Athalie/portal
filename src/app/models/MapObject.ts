export interface IMapObject {
  client_id: string;
  client_name: string;
  client_objects: IClientObject[]
}

interface IClientObject {
  id: string
  name: string
  parameters_value: IParameter[]
}

interface IParameter {
  parameter_id: string
  parameter_name: string
  parameter_system_name: string
  value: string;
}
