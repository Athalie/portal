declare const L: any;

export const blueIcon = new L.Icon({
	iconUrl: 'assets/global/vendor/leaflet/icons/img/marker-icon-2x-blue.png',
	shadowUrl: 'assets/global/vendor/leaflet/icons/img/marker-shadow.png',
	iconSize: [25, 41],
	iconAnchor: [12, 41],
	popupAnchor: [1, -34],
	shadowSize: [41, 41]
});

export const redIcon = new L.Icon({
	iconUrl: 'assets/global/vendor/leaflet/icons/img/marker-icon-2x-red.png',
	shadowUrl: 'assets/global/vendor/leaflet/icons/img/marker-shadow.png',
	iconSize: [25, 41],
	iconAnchor: [12, 41],
	popupAnchor: [1, -34],
	shadowSize: [41, 41]
});

export const greenIcon = new L.Icon({
	iconUrl: 'assets/global/vendor/leaflet/icons/img/marker-icon-2x-green.png',
	shadowUrl: 'assets/global/vendor/leaflet/icons/img/marker-shadow.png',
	iconSize: [25, 41],
	iconAnchor: [12, 41],
	popupAnchor: [1, -34],
	shadowSize: [41, 41]
});

export const orangeIcon = new L.Icon({
	iconUrl: 'assets/global/vendor/leaflet/icons/img/marker-icon-2x-orange.png',
	shadowUrl: 'assets/global/vendor/leaflet/icons/img/marker-shadow.png',
	iconSize: [25, 41],
	iconAnchor: [12, 41],
	popupAnchor: [1, -34],
	shadowSize: [41, 41]
});

export const yellowIcon = new L.Icon({
	iconUrl: 'assets/global/vendor/leaflet/icons/img/marker-icon-2x-yellow.png',
	shadowUrl: 'assets/global/vendor/leaflet/icons/img/marker-shadow.png',
	iconSize: [25, 41],
	iconAnchor: [12, 41],
	popupAnchor: [1, -34],
	shadowSize: [41, 41]
});

export const violetIcon = new L.Icon({
	iconUrl: 'assets/global/vendor/leaflet/icons/img/marker-icon-2x-violet.png',
	shadowUrl: 'assets/global/vendor/leaflet/icons/img/marker-shadow.png',
	iconSize: [25, 41],
	iconAnchor: [12, 41],
	popupAnchor: [1, -34],
	shadowSize: [41, 41]
});

export const greyIcon = new L.Icon({
	iconUrl: 'assets/global/vendor/leaflet/icons/img/marker-icon-2x-grey.png',
	shadowUrl: 'assets/global/vendor/leaflet/icons/img/marker-shadow.png',
	iconSize: [25, 41],
	iconAnchor: [12, 41],
	popupAnchor: [1, -34],
	shadowSize: [41, 41]
});

export const blackIcon = new L.Icon({
	iconUrl: 'assets/global/vendor/leaflet/icons/img/marker-icon-2x-black.png',
	shadowUrl: 'assets/global/vendor/leaflet/icons/img/marker-shadow.png',
	iconSize: [25, 41],
	iconAnchor: [12, 41],
	popupAnchor: [1, -34],
	shadowSize: [41, 41]
});
